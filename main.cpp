#include "Player.h"
#include <vector>
#include "Serializer.h"
#include <iostream>
#include <string>
#include "Deserializer.h"

/*
Ce main a pour objectif de copier les données d'une instance de "Player" dans une autre.
playerinstance1 et playerinstance2 sont des instances de "Player" qui sont sur deux ordinateurs différents.
Le "buffer" renvoyé par ser.getContainer() sur l'ordinateur "local" est voué à être envoyé sur le réseau puis utilisé par une instance de "Deserializer" sur l'ordinateur distant.
Ce dernier peut alors récupérer les informations sur le joueur de l'ordinateur "local" et remplacer les informations de son instance de "Player"
*/

void main()
{
    uqac::serializer::Serializer ser = uqac::serializer::Serializer();

    uqac::game::Player playerinstance1 = uqac::game::Player(uqac::serializer::Vector3D<float>(-500.345f, 0.2f, 100.1f), uqac::serializer::Quaternion(0.5f, 0.5f, 0.5f, 0.5f), uqac::serializer::Vector3D<float>(4.0f, 5.0f, 6.0f), 100, 50, 99998.2f, std::string("Monsieur XCV"));
    std::cout << "Instance de instance1 (modele) avant serialisation : " << std::endl << playerinstance1 << std::endl;
    playerinstance1.Write(&ser);

    //char *buffer = ser.getContainer();

    std::cout << "----------------" << std::endl;
    uqac::game::Player playerinstance2 = uqac::game::Player(uqac::serializer::Vector3D<float>(0.0f, 4.0f, 5.0f), uqac::serializer::Quaternion(0.0f, 0.7071f, 0.0f, 0.7071f), uqac::serializer::Vector3D<float>(1.0f, 2.0f, 3.0f), 70, 12, 120.9987f, std::string("Madame YUI"));
    std::cout << "Instance de instance2 avant deserialisation de instance1 : " << std::endl << playerinstance2 << std::endl;
    std::cout << "++++++++++++++++" << std::endl;

    uqac::serializer::Deserializer des = uqac::serializer::Deserializer(ser.getContainer(), ser.getContainerSize());
    
    playerinstance2.Read(&des);
    std::cout << "Instance de instance2 apres deserialisation de instance1 et affectation de ses donnees: " << std::endl
              << playerinstance2 << std::endl;
}