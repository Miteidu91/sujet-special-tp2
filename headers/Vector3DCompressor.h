#pragma once

#include "Compressor.h"
#include "Framework.h"
#include "FloatCompressor.h"
#include "Serializer.h"

namespace uqac::serializer
{
    class Vector3DCompressor
    {
    private:
        Vector3D<float> m_min;
        Vector3D<float> m_max;
        Vector3D<int> m_precision;

    public:
        Vector3DCompressor(Vector3D<float> i_min, Vector3D<float> i_max, Vector3D<int> i_precision) : 
            m_min(i_min), m_max(i_max), m_precision(i_precision)
        {};
        ~Vector3DCompressor() = default;
        void Compress(Serializer *s, Vector3D<float> value);
        Vector3D<float> Decompress(Deserializer *d);
    };
}
