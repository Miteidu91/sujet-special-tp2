#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "Serializer.h"

namespace uqac::serializer
{
    template <typename T>
    class Compressor
    {
    public:
        virtual char* Compress(Serializer *s, T value) = 0;
    };
}