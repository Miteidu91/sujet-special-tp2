#pragma once

#include <iostream>
#include <string>
#include "Framework.h"

namespace uqac::game
{
	class Player {
		private:
			uqac::serializer::Vector3D<float> m_position;
			uqac::serializer::Quaternion m_rotation;
			uqac::serializer::Vector3D<float> m_taille;
			int m_vie;
			int m_armure;
			float m_argent;
			std::string m_nom;
			void WritePosition(uqac::serializer::Serializer *s);
			void WriteRotation(uqac::serializer::Serializer *s);
			void WriteTaille(uqac::serializer::Serializer *s);
			void WriteNom(uqac::serializer::Serializer *s);
			uqac::serializer::Vector3D<float> ReadPosition(uqac::serializer::Deserializer *des);
			uqac::serializer::Quaternion ReadRotation(uqac::serializer::Deserializer *des);
			uqac::serializer::Vector3D<float> ReadTaille(uqac::serializer::Deserializer *des);
			std::string ReadNom(uqac::serializer::Deserializer *des);

		public:
			Player(uqac::serializer::Vector3D<float> position, uqac::serializer::Quaternion rotation, uqac::serializer::Vector3D<float> taille, int vie, int armure, float argent, std::string nom) :
				m_position(position), m_rotation(rotation), m_taille(taille), m_vie(vie), m_armure(armure), m_argent(argent)
			{
				SetNom(nom);
			}

			// Getters/Setters
			uqac::serializer::Vector3D<float> GetPosition() const;
			void SetPosition(uqac::serializer::Vector3D<float> _position);
			uqac::serializer::Quaternion GetRotation() const;
			void SetRotation(uqac::serializer::Quaternion _rotation);
			uqac::serializer::Vector3D<float> GetTaille() const;
			void SetTaille(uqac::serializer::Vector3D<float> _taille);
			int GetVie() const;
			void SetVie(int _vie);
			int GetArmure() const;
			void SetArmure(int _armure);
			float GetArgent() const;
			void SetArgent(float _argent);
			std::string GetNom() const;
			void SetNom(std::string _nom);

			void Player::Write(uqac::serializer::Serializer* s);
			void Player::Read(uqac::serializer::Deserializer* d);
	};

	// << operator
	std::ostream &operator<<(std::ostream &os, const Player &player);
}