#pragma once

#include "Compressor.h"
#include "Framework.h"
#include "Serializer.h"

namespace uqac::serializer
{
    class QuaternionCompressor
    {
    private:
        float m_min = -0.707f;
        float m_max = 0.707f;
        float m_maxRange = 1.414f;
        int m_precision = 3;

    public:
        QuaternionCompressor() = default;
        ~QuaternionCompressor() = default;
        void Compress(Serializer *s, Quaternion value);
        Quaternion Decompress(Deserializer *des);
    };
}
