#pragma once

#include <cstdio>
#include <string>
#include <algorithm>
#include "Serializer.h"
#include "Deserializer.h"

namespace uqac::serializer
{
    enum UintValue
    {
        UINT8V,
        UINT16V,
        UINT32V,
        UINT64V
    };
    
    //toUnsigned a coder
    struct Quaternion
    {
        float x, y, z, w;

        Quaternion(float _x, float _y, float _z, float _w)
        {
            this->x = std::max(-1.f, std::min(1.f,_x));
            this->y = std::max(-1.f, std::min(1.f,_y));
            this->z = std::max(-1.f, std::min(1.f,_z));
            this->w = std::max(-1.f, std::min(1.f,_w));
        }

        std::string ToString()
        {
            return "(" + std::to_string(x) + ", " + std::to_string(y) + ", " + std::to_string(z) + ", " + std::to_string(w) + ")";
        }

        void Write(Serializer* s);
        void Read(Deserializer* d);
    };

    template <typename T>
    struct Vector3D
    {
        T x, y, z;

        Vector3D() : x((T)0), y((T)0), z((T)0)
        {

        }

        Vector3D(T _x, T _y, T _z) : x(_x), y(_y), z(_z)
        {

        }

        std::string ToString()
        {
            return "(" + std::to_string(x) + ", " + std::to_string(y) + ", " + std::to_string(z) + ")";
        }

        void Write(Serializer* s);
        template <typename T>
        void Read(Deserializer* d);
    };

    // TODO : operateur << pour Quaternion et Vector3D

    enum class PlateformEndianess {
        BigEndian,
        LittleEndian
    };

    constexpr PlateformEndianess DetectEndianess() {
        union {
            uint32_t i;
            char c[4];
        } testEndian = { 0x01020304 };

        return (testEndian.c[0] == 1) ? PlateformEndianess::BigEndian : PlateformEndianess::LittleEndian;
    }

    template <typename T>
    void SwaptEndian(T& val) {
        union U {
            T val;
            std::array<std::uint8_t, sizeof(T)> raw;
        } src, dst;

        src.val = val;
        std::reverse_copy(src.raw.begin(), src.raw.end(), dst.raw.begin());
        val = dst.val;
    }

    UintValue getUintNeeded(int value);
}