#pragma once

#include "Framework.h"
#include "Serializer.h"

namespace uqac::serializer
{
    class IntCompressor
    {
    private:
        int m_min;
        int m_max;
        int m_maxRange;

    public:
        IntCompressor(int min, int max);
        ~IntCompressor() = default;
        void Compress(Serializer *s, int value);
        int Decompress(Deserializer *d);
    };
}