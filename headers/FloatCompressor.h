#pragma once

#include "Compressor.h"
#include "Framework.h"
#include "Serializer.h"

namespace uqac::serializer
{
    class FloatCompressor
    {
    private:
        float m_min;
        float m_max;
        float m_maxRange;
        int m_precision;

    public:
        FloatCompressor(float min, float max, int precision);
        ~FloatCompressor() = default;
        void Compress(Serializer *s, float value);
        float Decompress(Deserializer *d);
    };
}
