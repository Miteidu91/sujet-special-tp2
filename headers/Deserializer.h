#pragma once

#include <vector>
#include <string>
namespace uqac::serializer
{
    class Deserializer
    {
    private:
        const char* m_container_buffer;
        int m_taille = 0;
        int m_position = 0;

    public:
        Deserializer(const char* buffer, int size);
        ~Deserializer() = default;

        const char* getBuffer() const;
        const int getPosition() const;

        template <typename T, std::enable_if_t<std::is_arithmetic_v<std::remove_reference_t<T>>> * = nullptr>
        T Deserialize()
        {
            int sizeOfType = sizeof(T);
            if (m_taille < m_position + sizeOfType)
            {
                std::cout << "erreur deserialization" << std::endl;
                return 0;
            }

            T val;
            memcpy(&val, (m_container_buffer + m_position), sizeOfType);
            m_position += sizeOfType;
            return val;
        }
    };
}