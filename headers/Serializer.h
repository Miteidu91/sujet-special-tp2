#pragma once

#include <vector>

namespace uqac::serializer
{
	class Serializer
	{
	private:
		std::vector<char> m_container;
		int m_position;

	public:
		Serializer();
		Serializer(int size);
		~Serializer() = default;

		template <typename T, std::enable_if_t<std::is_arithmetic_v<std::remove_reference_t<T>>> * = nullptr>//erreur ici
		void Serialize(T object)
		{
			unsigned int sizeOfObject = sizeof(object);

			if (m_container.size() < m_position + sizeOfObject)
				m_container.resize(m_position + sizeOfObject);

			memcpy(m_container.data() + m_position, &object, sizeOfObject);
			m_position += sizeOfObject;
		}

		// getters
		char *getContainer();
		int getContainerSize();
		const int getPosition() const;

	};
}