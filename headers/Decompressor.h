#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "Deserializer.h"

namespace uqac::serializer
{
    template <typename T>
    class Decompressor
    {
    public:
        virtual char* Decompress(Deserializer *d) = 0;
    };
}