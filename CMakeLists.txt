cmake_minimum_required(VERSION 3.14)
project(Serialization)
set(CMAKE_CXX_STANDARD 17)

add_executable(Main main.cpp)

include_directories(headers)
add_subdirectory(sources)