# Sujet Spécial TP2

TP2 sujet spécial en informatique II

## Membres du groupe :

* Basile Bonicel
* Mathias Durand
* Théo Loubet

## Notes :
* Le "main" a pour objectif de copier les données d'une instance de "Player" dans une autre.  
* playerinstance1 et playerinstance2 sont des instances de "Player" qui sont sur deux ordinateurs différents.  
* Le "buffer" renvoyé par Serializer::getContainer() sur l'ordinateur "local" est voué à être envoyé sur le réseau puis utilisé par une instance de "Deserializer" sur un ordinateur distant.  
* Ce dernier peut alors récupérer les informations sur le joueur de l'ordinateur "local" et écraser les informations de son instance de "Player".