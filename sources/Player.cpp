#pragma once

#include "Player.h"
#include "Framework.h"
#include "Vector3DCompressor.h"
#include "IntCompressor.h"
#include "QuaternionCompressor.h"
#include "FloatCompressor.h"

// Getters / Setters
namespace uqac::game
{
	uqac::serializer::Vector3D<float> Player::GetPosition() const
	{
		return this->m_position;
	}

	void Player::SetPosition(uqac::serializer::Vector3D<float> _position)
	{
		this->m_position = _position;
	}

	uqac::serializer::Quaternion Player::GetRotation() const
	{
		return this->m_rotation;
	}

	void Player::SetRotation(uqac::serializer::Quaternion _rotation)
	{
		this->m_rotation = _rotation;
	}

	uqac::serializer::Vector3D<float> Player::GetTaille() const
	{
		return this->m_taille;
	}

	void Player::SetTaille(uqac::serializer::Vector3D<float> _taille)
	{
		this->m_taille = _taille;
	}

	int Player::GetVie() const
	{
		return this->m_vie;
	}

	void Player::SetVie(int _vie)
	{
		this->m_vie = _vie;
	}

	int Player::GetArmure() const
	{
		return this->m_armure;
	}

	void Player::SetArmure(int _armure)
	{
		this->m_armure = _armure;
	}

	float Player::GetArgent() const
	{
		return this->m_argent;
	}

	void Player::SetArgent(float _argent)
	{
		this->m_argent = _argent;
	}

	std::string Player::GetNom() const
	{
		return this->m_nom;
	}

	void Player::SetNom(std::string _nom)
	{
		if(_nom.length() > 128)
		{
			std::cout << "nom trop grand. Troncature du nom." << std::endl;
		}
		this->m_nom = _nom.substr(0, 128);
	}

	// operator <<
	std::ostream& operator<<(std::ostream& os, const Player& player)
	{
		os << "Position : " << player.GetPosition().ToString() << ", Rotation : " << player.GetRotation().ToString() << ", Taille : " << player.GetTaille().ToString() << ", Vie : " << player.GetVie() << ", Armure : " << player.GetArmure() << ", Argent : " << player.GetArgent() << ", Nom : " << player.GetNom();
		return os;
	}

	void Player::Write(uqac::serializer::Serializer* s)
	{
		WritePosition(s);
		WriteRotation(s);
		WriteTaille(s);
		uqac::serializer::IntCompressor vie_comp = uqac::serializer::IntCompressor::IntCompressor(0, 300);
		vie_comp.Compress(s, m_vie);
		uqac::serializer::IntCompressor armure_comp = uqac::serializer::IntCompressor::IntCompressor(0, 50);
		armure_comp.Compress(s, m_armure);
		uqac::serializer::FloatCompressor argent_comp = uqac::serializer::FloatCompressor::FloatCompressor(-99999.99f, 99999.99f, 3);
		argent_comp.Compress(s, m_argent);
		WriteNom(s);
	}

	void Player::Read(uqac::serializer::Deserializer* des)
	{
		m_position = ReadPosition(des);
		m_rotation = ReadRotation(des);
		m_taille = ReadTaille(des);
		uqac::serializer::IntCompressor vie_decomp = uqac::serializer::IntCompressor::IntCompressor(0, 300);
		m_vie = vie_decomp.Decompress(des);
		uqac::serializer::IntCompressor armure_decomp = uqac::serializer::IntCompressor::IntCompressor(0, 50);
		m_armure = armure_decomp.Decompress(des);
		uqac::serializer::FloatCompressor argent_comp = uqac::serializer::FloatCompressor::FloatCompressor(-99999.99f, 99999.99f, 3);
		m_argent = argent_comp.Decompress(des);
		m_nom = ReadNom(des);
	}

	void Player::WritePosition(uqac::serializer::Serializer *s)
	{
		uqac::serializer::Vector3D<float> v_min = uqac::serializer::Vector3D<float>::Vector3D(-500, -500, 0);
		uqac::serializer::Vector3D<float> v_max = uqac::serializer::Vector3D<float>::Vector3D(500, 500, 100);
		uqac::serializer::Vector3D<int> v_precision = uqac::serializer::Vector3D<int>::Vector3D(3, 3, 3);
		uqac::serializer::Vector3DCompressor comp_pos = uqac::serializer::Vector3DCompressor::Vector3DCompressor(v_min, v_max, v_precision);
		comp_pos.Compress(s, m_position);
	}

	void Player::WriteRotation(uqac::serializer::Serializer *s)
	{
		uqac::serializer::QuaternionCompressor comp_rot = uqac::serializer::QuaternionCompressor::QuaternionCompressor();
		comp_rot.Compress(s, m_rotation);
	}

	void Player::WriteTaille(uqac::serializer::Serializer *s)
	{
		uqac::serializer::Vector3D<float> v_min = uqac::serializer::Vector3D<float>::Vector3D(0, 0, 0);
		uqac::serializer::Vector3D<float> v_max = uqac::serializer::Vector3D<float>::Vector3D(10, 10, 10);
		uqac::serializer::Vector3D<int> v_precision = uqac::serializer::Vector3D<int>::Vector3D(3, 3, 3);
		uqac::serializer::Vector3DCompressor comp_taille = uqac::serializer::Vector3DCompressor::Vector3DCompressor(v_min, v_max, v_precision);
		comp_taille.Compress(s, m_taille);
	}

	void Player::WriteNom(uqac::serializer::Serializer *s)
	{
		s->Serialize((uint8_t) m_nom.length());
		for (char &c : m_nom)
		{
			s->Serialize((uint8_t) c);
		}
	}

	uqac::serializer::Vector3D<float> Player::ReadPosition(uqac::serializer::Deserializer *des)
	{
		uqac::serializer::Vector3D<float> v_min = uqac::serializer::Vector3D<float>::Vector3D(-500, -500, 0);
		uqac::serializer::Vector3D<float> v_max = uqac::serializer::Vector3D<float>::Vector3D(500, 500, 100);
		uqac::serializer::Vector3D<int> v_precision = uqac::serializer::Vector3D<int>::Vector3D(3, 3, 3);
		uqac::serializer::Vector3DCompressor decomp_pos = uqac::serializer::Vector3DCompressor::Vector3DCompressor(v_min, v_max, v_precision);
		return decomp_pos.Decompress(des);
	}
	uqac::serializer::Quaternion Player::ReadRotation(uqac::serializer::Deserializer *des)
	{
		uqac::serializer::QuaternionCompressor decomp_rot = uqac::serializer::QuaternionCompressor::QuaternionCompressor();
		return decomp_rot.Decompress(des);
	}
	uqac::serializer::Vector3D<float> Player::ReadTaille(uqac::serializer::Deserializer *des)
	{
		uqac::serializer::Vector3D<float> v_min = uqac::serializer::Vector3D<float>::Vector3D(0, 0, 0);
		uqac::serializer::Vector3D<float> v_max = uqac::serializer::Vector3D<float>::Vector3D(10, 10, 10);
		uqac::serializer::Vector3D<int> v_precision = uqac::serializer::Vector3D<int>::Vector3D(3, 3, 3);
		uqac::serializer::Vector3DCompressor decomp_taille = uqac::serializer::Vector3DCompressor::Vector3DCompressor(v_min, v_max, v_precision);
		return decomp_taille.Decompress(des);
	}

	std::string Player::ReadNom(uqac::serializer::Deserializer *des)
	{
		int nbChars = des->Deserialize<uint8_t>();
		std::string res = "";
		for (int i = 0; i<nbChars; i++)
		{
			res += (char) des->Deserialize<uint8_t>();
		}
		return res;
	}
}
