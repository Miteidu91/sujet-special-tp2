#include "../headers/Framework.h"

namespace uqac::serializer
{
    UintValue getUintNeeded(int value)
    {
        if (value <= UINT8_MAX)
            return UintValue::UINT8V;
        if (value <= UINT16_MAX)
            return UintValue::UINT16V;
        if (value <= UINT32_MAX)
            return UintValue::UINT32V;
        return UintValue::UINT64V;
    }
}