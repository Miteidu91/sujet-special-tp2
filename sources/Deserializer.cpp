#include "../headers/Deserializer.h"

namespace uqac::serializer
{
    Deserializer::Deserializer(const char* buffer, int size):
        m_taille(size), m_container_buffer(buffer), m_position(0)
    {
    }

    const char* Deserializer::getBuffer() const
    {
        return m_container_buffer;
    }

    const int Deserializer::getPosition() const
    {
        return m_position;
    }
}