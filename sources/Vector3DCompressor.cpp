#include <iostream>
#include <string>
#include "../headers/Vector3DCompressor.h"
#include "../headers/FloatCompressor.h"
#include "../headers/Framework.h"

namespace uqac::serializer
{
    void Vector3DCompressor::Compress(Serializer *s, Vector3D<float> value)
    {
        FloatCompressor fc1 = FloatCompressor::FloatCompressor(m_min.x, m_max.x, m_precision.x);
        FloatCompressor fc2 = FloatCompressor::FloatCompressor(m_min.y, m_max.y, m_precision.y);
        FloatCompressor fc3 = FloatCompressor::FloatCompressor(m_min.z, m_max.z, m_precision.z);
        fc1.Compress(s, value.x);
        fc2.Compress(s, value.y);
        fc3.Compress(s, value.z);
    }

    Vector3D<float> Vector3DCompressor::Decompress(Deserializer *d)
    {
        FloatCompressor fdc1 = FloatCompressor::FloatCompressor(m_min.x, m_max.x, m_precision.x);
        FloatCompressor fdc2 = FloatCompressor::FloatCompressor(m_min.y, m_max.y, m_precision.y);
        FloatCompressor fdc3 = FloatCompressor::FloatCompressor(m_min.z, m_max.z, m_precision.z);
        float v1 = fdc1.Decompress(d);
        float v2 = fdc2.Decompress(d);
        float v3 = fdc3.Decompress(d);
        Vector3D<float> rv(v1, v2, v3);
        return rv;
    }
}