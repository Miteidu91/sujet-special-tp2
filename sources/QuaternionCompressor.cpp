#include <iostream>
#include <bitset>
#include <string>
#include "../headers/QuaternionCompressor.h"
#include "../headers/Framework.h"

namespace uqac::serializer
{
    void QuaternionCompressor::Compress(Serializer *s, Quaternion value)
    {
        float coords[3];
        int ignoredCoord;
        float maxValue = std::max({value.x, value.y, value.z, value.w});

        if (maxValue == value.x)
        {
            ignoredCoord = 0;
            coords[0] = value.y; 
            coords[1] = value.z; 
            coords[2] = value.w;
        }
        else if(maxValue == value.y){
            ignoredCoord = 1;
            coords[0] = value.x;
            coords[1] = value.z;
            coords[2] = value.w;
        }
        else if(maxValue == value.z){
            ignoredCoord = 2;
            coords[0] = value.x;
            coords[1] = value.y;
            coords[2] = value.w;
        }
        else if(maxValue == value.w){
            ignoredCoord = 3;
            coords[0] = value.x;
            coords[1] = value.y;
            coords[2] = value.z;
        }

        int intCoords[3];
        for (int i = 0; i < 3; ++i) {
            intCoords[i] = (int) ((coords[i] - m_min) * pow(10,m_precision) * 1023 / 1414);
        }

        uint32_t result = intCoords[2] | (intCoords[1] << 10) | (intCoords[0] << 20) | (ignoredCoord << 30);

        // L'affichage en bits, pour voir s'ils sont rangés correctement dans le uint32_t :
        //std::bitset<32> y(result);
        //std::cout << y << '\n';
        
        s->Serialize(result);
    }

    Quaternion QuaternionCompressor::Decompress(Deserializer *des)
    {
        uint32_t flex = des->Deserialize<uint32_t>();

        // L'affichage en bits, pour voir s'ils sont récupérés correctement par le deserialize et bien stockés dans le uint32_t :
        //std::bitset<32> y(flex);
        //std::cout << y << '\n';

        int ignoredVal = flex >> 30;
        float a = ((flex << 2) >> 22) * (1414.f / 1023.f) / pow(10, m_precision) + m_min;
        float b = ((flex << 12) >> 22) * (1414.f / 1023.f) / pow(10, m_precision) + m_min;
        float c = ((flex << 22) >> 22) * (1414.f / 1023.f) / pow(10, m_precision) + m_min;
        float d = sqrt(1 - (a * a + b * b + c * c));

        switch (ignoredVal)
        {
        case 0:
            return Quaternion(d, a, b, c);
            break;
        case 1:
            return Quaternion(a, d, b, c);
            break;
        case 2:
            return Quaternion(a, b, d, c);
            break;
        default:
            return Quaternion(a, b, c, d);
            break;
        }
    }
}