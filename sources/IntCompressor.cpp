#include <iostream>
#include <string>
#include "../headers/IntCompressor.h"
#include "../headers/Framework.h"
#include "../headers/Serializer.h"

namespace uqac::serializer
{
    void IntCompressor::Compress(Serializer *s, int value)
    {
        //ramene l'echelle a 0
        int nv = value - m_min;
        UintValue uintNeeded = getUintNeeded(m_maxRange);
        switch (uintNeeded)
        {
        case UintValue::UINT8V :
            s->Serialize((uint8_t)nv);
            break;
        case UintValue::UINT16V:
            s->Serialize((uint16_t)nv);
            break;
        case UintValue::UINT32V:
            s->Serialize((uint32_t)nv);
            break;
        default:
            s->Serialize((uint64_t)nv);
            break;
        }
    }

    int IntCompressor::Decompress(Deserializer *d)
    {
        int tmp;
        UintValue uintNeeded = getUintNeeded(m_maxRange);
        switch (uintNeeded)
        {
        case UintValue::UINT8V:
            tmp = d->Deserialize<uint8_t>();
            break;
        case UintValue::UINT16V:
            tmp = d->Deserialize<uint16_t>();
            break;
        case UintValue::UINT32V:
            tmp = d->Deserialize<uint32_t>();
            break;
        default:
            tmp = d->Deserialize<uint64_t>();
            break;
        }
        int rv = tmp + m_min;
        return rv;
    }

    IntCompressor::IntCompressor(int min, int max) : 
        m_min(min), m_max(max), m_maxRange(max - min)
    {
    }
}