#include "../headers/Serializer.h"
#include "../headers/Framework.h"

namespace uqac::serializer
{
	Serializer::Serializer()
	{
		m_position = 0;
	}

	Serializer::Serializer(int size)
	{
		m_position = 0;
		m_container.resize(size);
	}

	char* Serializer::getContainer()
	{
		return m_container.data();
	}

	int Serializer::getContainerSize()
	{
		return m_position;
	}

	const int Serializer::getPosition() const
	{
		return m_position;
	}
}