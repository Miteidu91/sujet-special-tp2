#include <iostream>
#include <string>
#include "../headers/FloatCompressor.h"
#include "../headers/Serializer.h"
#include "../headers/Framework.h"

namespace uqac::serializer
{
    void FloatCompressor::Compress(Serializer *s, float value)
    {
        // ramene l'echelle a 0
        int nv = (int) ((value - m_min) * pow(10,m_precision));
        UintValue uintNeeded = getUintNeeded(m_maxRange);
        switch (uintNeeded)
        {
        case UintValue::UINT8V:
            s->Serialize((uint8_t)nv);
            break;
        case UintValue::UINT16V:
            s->Serialize((uint16_t)nv);
            break;
        case UintValue::UINT32V:
            s->Serialize((uint32_t)nv);
            break;
        default:
            s->Serialize((uint64_t)nv);
            break;
        }
    }

    float FloatCompressor::Decompress(Deserializer *d)
    {
        int tmp;
        UintValue uintNeeded = getUintNeeded(m_maxRange);
        switch (uintNeeded)
        {
        case UintValue::UINT8V:
            tmp = d->Deserialize<uint8_t>();
            break;
        case UintValue::UINT16V:
            tmp = d->Deserialize<uint16_t>();
            break;
        case UintValue::UINT32V:
            tmp = d->Deserialize<uint32_t>();
            break;
        default:
            tmp = d->Deserialize<uint64_t>();
            break;
        }
        float rv = (float)((tmp / pow(10, m_precision)) + m_min);
        return rv;
    }

    FloatCompressor::FloatCompressor(float min, float max, int precision):
        m_min(min),
        m_max(max), m_precision(precision), m_maxRange((max-min) * pow(10,precision))
    {
    }
}